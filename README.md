# Tidsbanken WebAPI

A .NET WebAPI backend for Tidsbanken.

## Description

Tidsbanken is a software stack that provides a way to track vacation time in an organization. Tidsbanken consists of a .NET WebAPI backend and a React frontend. This is the backend repo.

## Entity Relationship Diagram (ERD)
In our repository you can also find an image of a ER-Diagram, which illustrates the relationship between our entity models, and also how our database is configured. The ERD can be found [here](/ER-Diagram.png).

## Running this backend locally

* Open project in visual studio
* Change DefaultConnection string in appsettings.json
* Add a jwt field in appsettings.json<br>
    example: <br>
    "Jwt": {<br>
        "Authority": "https://localhost:7228/swagger/index.html",<br>
        "Audience": "https://localhost:7228/swagger/index.html",<br>
        "Key": "My_SUPER_SECRET!CODE"
    }
* Run update-database in NuGet cli
* Run solution with GUI or cli

## Usage

This backend is hosted on Azure, along with [this frontend](https://gitlab.com/IdaTroan/frontend_tidsbanken). The live backend can be accessed at https://tidsbanken-backend.azurewebsites.net/api/ for example with postman. The live frontend can be accessed at https://tidsbanken.azurewebsites.net.

## Contributors

This API is developed by [Tore Kamsvåg](https://gitlab.com/Torekamsvag), [Kim-Reino Hjelde](https://gitlab.com/kimreinh) and [Ida Trøan](https://gitlab.com/IdaTroan)

## License

[MIT](LICENSE)
