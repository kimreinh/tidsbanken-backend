﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Tidsbanken.Models.Domain;

namespace Tidsbanken.Services
{
    public interface IAuthenticationService
    {
        public Task<User> LoginAsync(User user);

        public JwtSecurityToken GetToken(List<Claim> authClaim);
    }
}
