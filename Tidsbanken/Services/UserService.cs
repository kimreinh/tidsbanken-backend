﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using Tidsbanken.Data;
using Tidsbanken.Models.Domain;
using Tidsbanken.Tools;

namespace Tidsbanken.Services
{
    /// <summary>
    /// This class implements the IUserService interface and provides CRUD operations for the User model
    /// </summary>
    public class UserService : IUserService
    {
        private readonly TidsbankenDbContext _context;

        // Dependency injection
        public UserService(TidsbankenDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Async method to add a user to the database.
        /// </summary>
        /// <param name="user">A User object.</param>
        /// <returns></returns>
        public async Task<User> AddUserAsync(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return user;
        }

        /// <summary>
        /// Async method to delete a given user from the database.
        /// </summary>
        /// <param name="userId">The id of the user that is being deleted.</param>
        /// <returns></returns>
        public async Task DeleteUserAsync(int userId)
        {
            var user = await _context.Users.FindAsync(userId);
            if (user != null)
            {
                _context.Users.Remove(user);
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Async method to retrieve all users from the database.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            return await _context.Users
                .Include(u => u.VacationRequests)
                .Include(u => u.Comments)
                .Include(u => u.IneligiblePeriods)
                .Include(u => u.vacationRequestStatuses)
                .ToListAsync();
        }

        /// <summary>
        /// Async method to get all requests for a given user.
        /// </summary>
        /// <param name="userId">The id of the user that we get the requests from.</param>
        /// <returns></returns>
        public async Task<List<VacationRequest>> GetRequestsByUserAsync(int userId)
        {
            var requests = await _context.VacationRequests
                .Where(v => v.UserID == userId)
                .ToListAsync();

            return requests;
        }

        /// <summary>
        /// Async method to get a specific user.
        /// </summary>
        /// <param name="userId">The id of the user.</param>
        /// <returns></returns>
        public async Task<User> GetSpecificUserAsync(int userId)
        {
            return await _context.Users
                .Include(u => u.VacationRequests)
                .Include(u => u.Comments)
                .Include(u => u.IneligiblePeriods)
                .Include(u => u.vacationRequestStatuses)
                .FirstAsync(u => u.UserID == userId);
        }

        /// <summary>
        /// Async method to make a partial update of a user.
        /// </summary>
        /// <param name="userId">The id of the user to be updated.</param>
        /// <param name="userUpdate">A collection of operations to be performed on the target JSON document.</param>
        /// <returns></returns>
        public async Task PatchUserAsync(int userId, JsonPatchDocument<User> userUpdate)
        {
            var user = await _context.Users.FindAsync(userId);

            if (user != null)
            {
                userUpdate.ApplyTo(user);
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Async method to update a user.
        /// </summary>
        /// <param name="user">A User object.</param>
        /// <returns></returns>
        public async Task UpdateUserAsync(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Helper function to verify the existance of a user in the database.
        /// </summary>
        /// <param name="userId">The id of the user that is being checked.</param>
        /// <returns></returns>
        public bool UserExists(int userId)
        {
            return _context.Users.Any(e => e.UserID == userId);
        }

        /// <summary>
        /// Async method to change the password for a user.
        /// </summary>
        /// <param name="user">A user object.</param>
        /// <param name="newPassword">A new password string.</param>
        /// <returns></returns>
        public async Task ChangePasswordAsync(User user, string newPassword)
        {
            user.Password = Password.HashPassword(newPassword, user.Salt);
            await _context.SaveChangesAsync();

        }
    }
}
