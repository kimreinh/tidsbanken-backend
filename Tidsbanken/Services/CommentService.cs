﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using Tidsbanken.Data;
using Tidsbanken.Models.Domain;

namespace Tidsbanken.Services
{
    /// <summary>
    /// This class implements the ICommentService interface and provides CRUD operations for the Comment model
    /// </summary>
    public class CommentService : ICommentService
    {
        // TidsbankenDbContext instance to communicate with the database
        private readonly TidsbankenDbContext _context;

        // Dependency injection
        public CommentService(TidsbankenDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Method to check if a comment with the given id exists in the database.
        /// </summary>
        /// <param name="id">The id of the comment to check.</param>
        /// <returns>Returns a boolean value, which is either true or false.</returns>
        public bool CommentExists(int id)
        {
            return _context.Comments.Any(c => c.CommentID == id);
        }

        /// <summary>
        /// Async method to delete a comment in the database.
        /// </summary>
        /// <param name="commentId">The id of the comment that is being deleted.</param>
        /// <returns></returns>
        public async Task DeleteCommentAsync(int commentId)
        {
            var comment = await _context.Comments.FindAsync(commentId);
            if (comment != null)
            {
                _context.Comments.Remove(comment);
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Async method to retrieve all comments by a given requestId.
        /// </summary>
        /// <param name="requestId">The id of the request to retrieve comments from.</param>
        /// <returns></returns>
        public async Task<IEnumerable<Comment>> GetAllRequestCommentsAsync(int requestId)
        {
            return await _context.Comments
                .Where(c => c.VacationRequestID == requestId)
                .Include(c => c.VacationRequest)
                .OrderByDescending(c => c.Created)
                .ToListAsync();
        }

        /// <summary>
        /// Async method to retrieve a comment by id.
        /// </summary>
        /// <param name="commentId">The id of the comment to retrieve.</param>
        /// <returns></returns>
        public async Task<Comment> GetCommentAsync(int commentId)
        {
            return await _context.Comments.FirstAsync(c => c.CommentID == commentId);
        }

        /// <summary>
        /// Async method to add a comment.
        /// </summary>
        /// <param name="comment">A Comment object.</param>
        /// <returns></returns>
        public async Task PostCommentAsync(Comment comment)
        {
            _context.Comments.Add(comment);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Async method to update a comment.
        /// </summary>
        /// <param name="comment">A Comment object.</param>
        /// <returns></returns>
        public async Task PutCommentAsync(Comment comment)
        {
            _context.Entry(comment).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task<VacationRequest> GetVacationRequest(int requestId)
        {
            return await _context.VacationRequests.FirstAsync(v => v.VacationRequestID == requestId);
        }

        /// <summary>
        /// Async method to make a partial update of a comment.
        /// </summary>
        /// <param name="comment">A Comment object.</param>
        /// <param name="commentUpdate">A collection of operations to be performed on the target JSON document.</param>
        /// <returns></returns>
        public async Task PatchCommentAsync(Comment comment, JsonPatchDocument<Comment> commentUpdate)
        {
            DateTime created = comment.Created;
            DateTime? edited = comment.Edited;
            commentUpdate.ApplyTo(comment);
            comment.Created = created;
            comment.Edited = edited;
            await _context.SaveChangesAsync();
        }
    }
}
