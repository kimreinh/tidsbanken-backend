﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using Tidsbanken.Data;
using Tidsbanken.Models.Domain;

namespace Tidsbanken.Services
{
    public class IneligiblePeriodService : IIneligiblePeriodService
    {
        private readonly TidsbankenDbContext _context;

        // Dependency injection
        public IneligiblePeriodService(TidsbankenDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Delete ineligible vacation period from database.
        /// </summary>
        /// <param name="periodId"></param>
        /// <returns></returns>
        public async Task DeleteIneligiblePeriodAsync(int periodId)
        {
            var period = await _context.IneligiblePeriods.FindAsync(periodId);
            if (period != null)
            {
                _context.IneligiblePeriods.Remove(period);
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Get a single ineligible vacation period by period id.
        /// </summary>
        /// <param name="periodId"></param>
        /// <returns></returns>
        public async Task<IneligiblePeriod> GetIneligiblePeriodByIdAsync(int periodId)
        {
            return await _context.IneligiblePeriods.FirstAsync(p => p.IneligiblePeriodID == periodId);
        }

        /// <summary>
        /// Get all ineligible vacation periods ordered by period end descending.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<IneligiblePeriod>> GetIneligiblePeriodsAsync()
        {
            return await _context.IneligiblePeriods
                .OrderByDescending(p => p.PeriodEnd)
                .ToListAsync();
        }

        /// <summary>
        /// Add ineligible vacation period to database.
        /// </summary>
        /// <param name="ineligiblePeriod"></param>
        /// <returns></returns>
        public async Task PostIneligiblePeriodAsync(IneligiblePeriod ineligiblePeriod)
        {
            _context.IneligiblePeriods.Add(ineligiblePeriod);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Helper function to verify existence of ineligible vacation period in db.
        /// </summary>
        /// <param name="periodId"></param>
        /// <returns></returns>
        public bool IneligiblePeriodExists(int periodId)
        {
            return _context.IneligiblePeriods.Any(c => c.IneligiblePeriodID == periodId);
        }

        /// <summary>
        /// Partial update to ineligible vacation period in db.
        /// </summary>
        /// <param name="periodId"></param>
        /// <param name="periodUpdate"></param>
        /// <returns></returns>
        public async Task PatchIneligiblePeriodAsync(int periodId, JsonPatchDocument<IneligiblePeriod> periodUpdate)
        {
            var period = await _context.IneligiblePeriods.FindAsync(periodId);

            if (period != null)
            {
                periodUpdate.ApplyTo(period);
                await _context.SaveChangesAsync();
            }
        }
    }
}
