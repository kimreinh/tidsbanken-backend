﻿using Microsoft.AspNetCore.JsonPatch;
using Tidsbanken.Models.Domain;

namespace Tidsbanken.Services
{
    public interface IIneligiblePeriodService
    {
        public Task<IEnumerable<IneligiblePeriod>> GetIneligiblePeriodsAsync();
        public Task<IneligiblePeriod> GetIneligiblePeriodByIdAsync(int periodId);
        public Task PostIneligiblePeriodAsync(IneligiblePeriod ineligiblePeriod);
        public Task DeleteIneligiblePeriodAsync(int periodId);
        public bool IneligiblePeriodExists(int id);
        public Task PatchIneligiblePeriodAsync(int periodId, JsonPatchDocument<IneligiblePeriod> periodUpdate);
    }
}
