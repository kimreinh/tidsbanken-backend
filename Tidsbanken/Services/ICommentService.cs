﻿using Microsoft.AspNetCore.JsonPatch;
using Tidsbanken.Models.Domain;

namespace Tidsbanken.Services
{
    public interface ICommentService
    {
        public Task<IEnumerable<Comment>> GetAllRequestCommentsAsync(int requestId);
        public Task<Comment> GetCommentAsync(int commentId);
        public Task PutCommentAsync(Comment comment);
        public Task PostCommentAsync(Comment comment);
        public Task DeleteCommentAsync(int commentId);
        public Task<VacationRequest> GetVacationRequest(int requestId);
        public bool CommentExists(int id);
        public Task PatchCommentAsync(Comment comment, JsonPatchDocument<Comment> commentUpdate);

    }
}
