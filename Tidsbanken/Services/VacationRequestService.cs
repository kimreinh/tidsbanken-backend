﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using Tidsbanken.Data;
using Tidsbanken.Models.Domain;

namespace Tidsbanken.Services
{
    /// <summary>
    /// This class implements the IVacationRequestService interface and provides CRUD operations for the VacationRequest model
    /// </summary>
    public class VacationRequestService : IVacationRequestService
    {
        private readonly TidsbankenDbContext _context;

        // Dependency injection
        public VacationRequestService(TidsbankenDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Async method to add a new vacation request to the database.
        /// </summary>
        /// <param name="vacationRequest">A VacationRequest object.</param>
        /// <returns></returns>
        public async Task<VacationRequest> AddVacationRequestAsync(VacationRequest vacationRequest)
        {
            VacationRequestStatus vacationRequestStatus = new VacationRequestStatus();
            vacationRequestStatus.Status = Status.Pending;  // Specifying a pending status on creation of the request.
            _context.VacationRequestsStatus.Add(vacationRequestStatus);
            await _context.SaveChangesAsync();
            
            vacationRequest.VacationRequestStatus = vacationRequestStatus;
            vacationRequest.VacationRequestStatusID = vacationRequest.VacationRequestStatus.VacationRequestStatusID;
            
            vacationRequest.MaxVacationLengthID = 1;
            _context.VacationRequests.Add(vacationRequest);

            // Save changes to the database.
            await _context.SaveChangesAsync();
            
            return vacationRequest;
        }

        /// <summary>
        /// Async method to delete a vacation request from the database.
        /// </summary>
        /// <param name="requestId">The id of the request that is being deleted.</param>
        /// <returns></returns>
        public async Task DeleteVacationRequestAsync(int requestId)
        {
            var vacationRequest = await _context.VacationRequests.FindAsync(requestId);
            var vacationRequestStatus = await _context.VacationRequestsStatus.FindAsync(vacationRequest.VacationRequestStatusID);
            vacationRequestStatus.UserID = null;
            if (vacationRequest != null)
            {
                // Remove the request and save the changes to the database.
                _context.VacationRequestsStatus.Remove(vacationRequestStatus);
                _context.VacationRequests.Remove(vacationRequest);
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Async method to get a specific vacation request from the database.
        /// </summary>
        /// <param name="requestId">The id of the request we are trying to fetch.</param>
        /// <returns></returns>
        public async Task<VacationRequest> GetSpecificVacationRequestAsync(int requestId)
        {
            return await _context.VacationRequests.FirstAsync(v => v.VacationRequestID == requestId);
        }

        /// <summary>
        /// Async method to get all vacation requests.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<VacationRequest>> GetVacationRequestsAsync()
        {
            return await _context.VacationRequests
                .Include(v => v.Comments)
                .Include(v => v.User)
                .Include(v => v.VacationRequestStatus)
                .ToListAsync();
        }

        /// <summary>
        /// Async method to make a partial update of a given vacation request.
        /// </summary>
        /// <param name="requestId">The id of the request that is being updated.</param>
        /// <param name="requestUpdate">A collection of operations to be performed on the target JSON document.</param>
        /// <returns></returns>
        public async Task PatchVacationRequestAsync(int requestId, JsonPatchDocument<VacationRequest> requestUpdate)
        {
            var vacationRequest = await _context.VacationRequests.FindAsync(requestId);
            if (vacationRequest != null)
            {
                requestUpdate.ApplyTo(vacationRequest);
                // Save the changes to the database.
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Async method to update a vacation request.
        /// </summary>
        /// <param name="vacationRequest">A VacationRequest object.</param>
        /// <returns></returns>
        public async Task UpdateVacationRequestAsync(VacationRequest vacationRequest)
        {
            _context.Entry(vacationRequest).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Helper function which verifies if a vacation request exists in the database.
        /// </summary>
        /// <param name="requestId">The id of the request that is checked.</param>
        /// <returns></returns>
        public bool VacationRequestExists(int requestId)
        {
            return _context.VacationRequests.Any(e => e.VacationRequestID == requestId);
        }

        /// <summary>
        /// Helper function to get the max vacation length variable from the db.
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetMaxVacationLength() 
        {
            MaxVacationLength vacationLength = await _context.MaxVacationLength.FirstAsync(v => v.MaxVacationLengthID == 1);
            return vacationLength.LengthInDays;
        }

        /// <summary>
        /// Async method to get the status for a given vacation request.
        /// </summary>
        /// <param name="requestId">The id of the vacation request.</param>
        /// <returns></returns>
        public async Task<VacationRequestStatus> GetVacationRequestStatusAsync(int requestId)
        {
            VacationRequest req = await _context.VacationRequests.FirstAsync(v => v.VacationRequestID == requestId);
            return await _context.VacationRequestsStatus.FirstAsync(s => s.VacationRequestStatusID == req.VacationRequestStatusID);
        }

        /// <summary>
        /// Async method to make a partial update of the request status.
        /// </summary>
        /// <param name="status">A VacationRequestStatus object.</param>
        /// <param name="statusUpdate">A collection of operations to be performed on the target JSON document.</param>
        /// <returns></returns>
        public async Task UpdateRequestStatusAsync(VacationRequestStatus status, JsonPatchDocument<VacationRequestStatus> statusUpdate)
        {
            statusUpdate.ApplyTo(status);
            // Save the changes to the database.
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Async method to update the max vacation length.
        /// </summary>
        /// <param name="len">An object that represents the length in days.</param>
        /// <returns></returns>
        public async Task UpdateMaxVacationLengthAsync(int len)
        {
            MaxVacationLength maxlen = await _context.MaxVacationLength.FirstAsync(l => l.MaxVacationLengthID == 1);
            maxlen.LengthInDays = len;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Async method to get the max vacation length.
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetMaxVacationLengthAsync()
        {
            MaxVacationLength maxlen = await _context.MaxVacationLength.FirstAsync(l => l.MaxVacationLengthID == 1);
            return maxlen.LengthInDays;
        }
    }
}
