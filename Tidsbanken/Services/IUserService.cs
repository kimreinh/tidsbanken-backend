﻿using Microsoft.AspNetCore.JsonPatch;
using Tidsbanken.Models.Domain;

namespace Tidsbanken.Services
{
    public interface IUserService
    {
        public Task<IEnumerable<User>> GetAllUsersAsync();
        public Task<User> GetSpecificUserAsync(int userId);
        public Task UpdateUserAsync(User user);
        public Task<User> AddUserAsync(User user);
        public Task DeleteUserAsync(int userId);
        public bool UserExists(int userId);
        public Task PatchUserAsync(int userId, JsonPatchDocument<User> userUpdate);
        public Task<List<VacationRequest>> GetRequestsByUserAsync(int userId);
        public Task ChangePasswordAsync(User user, string newPassword);
    }
}
