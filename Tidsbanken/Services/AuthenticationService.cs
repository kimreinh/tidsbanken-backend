﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Tidsbanken.Data;
using Tidsbanken.Models.Domain;

namespace Tidsbanken.Services
{
    /// <summary>
    /// This class implements the IAuthenticationService interface and provides authentication-related functionality.
    /// </summary>
    public class AuthenticationService : IAuthenticationService
    {
        private readonly TidsbankenDbContext _context;
        private readonly IConfiguration _configuration;

        // Dependency injection, where external dependencies are passed into the class
        public AuthenticationService(TidsbankenDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        /// <summary>
        /// Implementation of an asynchronous login method.
        /// </summary>
        /// <param name="user">A user object</param>
        /// <returns>Returns a user object from the database.</returns>
        public async Task<User> LoginAsync(User user)
        {
            var dbUser = await _context.Users
                .Where(u => u.Email == user.Email)
                .FirstAsync();
            
            return dbUser;
        }

        /// <summary>
        /// Implementation of a method that generates a JWT (JSON Web Token) security token using the provided list of claims.
        /// </summary>
        /// <param name="authClaim"></param>
        /// <returns></returns>
        public JwtSecurityToken GetToken(List<Claim> authClaim)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

            var token = new JwtSecurityToken(
                issuer: _configuration["Jwt:Authority"],
                audience: _configuration["Jwt:Audience"],
                expires: DateTime.Now.AddMinutes(20),
                claims: authClaim,
                signingCredentials: new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256)
                );

            return token;
        }
    }
}
