﻿using Microsoft.AspNetCore.JsonPatch;
using Tidsbanken.Models.Domain;

namespace Tidsbanken.Services
{
    public interface IVacationRequestService
    {
        public Task<IEnumerable<VacationRequest>> GetVacationRequestsAsync();
        public Task<VacationRequest> GetSpecificVacationRequestAsync(int requestId);
        public Task UpdateVacationRequestAsync(VacationRequest vacationRequest);
        public Task<VacationRequest> AddVacationRequestAsync(VacationRequest vacationRequest);
        public Task DeleteVacationRequestAsync(int requestId);
        public bool VacationRequestExists(int requestId);
        public Task PatchVacationRequestAsync(int requestId, JsonPatchDocument<VacationRequest> requestUpdate);
        public Task<int> GetMaxVacationLength();
        public Task<VacationRequestStatus> GetVacationRequestStatusAsync(int requestId);
        public Task UpdateRequestStatusAsync(VacationRequestStatus status, JsonPatchDocument<VacationRequestStatus> statusUpdate);
        public Task UpdateMaxVacationLengthAsync(int len);
        public Task<int> GetMaxVacationLengthAsync();
    }
}
