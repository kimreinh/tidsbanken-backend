﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tidsbanken.Migrations
{
    public partial class vacationStatusAndLengthDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Status",
                table: "VacationRequests",
                newName: "VacationRequestStatusID");

            migrationBuilder.AddColumn<int>(
                name: "MaxVacationLengthID",
                table: "VacationRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "MaxVacationLength",
                columns: table => new
                {
                    MaxVacationLengthID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LengthInDays = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaxVacationLength", x => x.MaxVacationLengthID);
                });

            migrationBuilder.CreateTable(
                name: "VacationRequestStatus",
                columns: table => new
                {
                    VacationRequestStatusID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Moderated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UserID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VacationRequestStatus", x => x.VacationRequestStatusID);
                    table.ForeignKey(
                        name: "FK_VacationRequestStatus_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID");
                });

            migrationBuilder.CreateIndex(
                name: "IX_VacationRequests_MaxVacationLengthID",
                table: "VacationRequests",
                column: "MaxVacationLengthID");

            migrationBuilder.CreateIndex(
                name: "IX_VacationRequests_VacationRequestStatusID",
                table: "VacationRequests",
                column: "VacationRequestStatusID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VacationRequestStatus_UserID",
                table: "VacationRequestStatus",
                column: "UserID");

            migrationBuilder.AddForeignKey(
                name: "FK_VacationRequests_MaxVacationLength_MaxVacationLengthID",
                table: "VacationRequests",
                column: "MaxVacationLengthID",
                principalTable: "MaxVacationLength",
                principalColumn: "MaxVacationLengthID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VacationRequests_VacationRequestStatus_VacationRequestStatusID",
                table: "VacationRequests",
                column: "VacationRequestStatusID",
                principalTable: "VacationRequestStatus",
                principalColumn: "VacationRequestStatusID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VacationRequests_MaxVacationLength_MaxVacationLengthID",
                table: "VacationRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_VacationRequests_VacationRequestStatus_VacationRequestStatusID",
                table: "VacationRequests");

            migrationBuilder.DropTable(
                name: "MaxVacationLength");

            migrationBuilder.DropTable(
                name: "VacationRequestStatus");

            migrationBuilder.DropIndex(
                name: "IX_VacationRequests_MaxVacationLengthID",
                table: "VacationRequests");

            migrationBuilder.DropIndex(
                name: "IX_VacationRequests_VacationRequestStatusID",
                table: "VacationRequests");

            migrationBuilder.DropColumn(
                name: "MaxVacationLengthID",
                table: "VacationRequests");

            migrationBuilder.RenameColumn(
                name: "VacationRequestStatusID",
                table: "VacationRequests",
                newName: "Status");
        }
    }
}
