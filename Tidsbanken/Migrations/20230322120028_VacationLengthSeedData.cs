﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tidsbanken.Migrations
{
    public partial class VacationLengthSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Moderated",
                table: "VacationRequestsStatus",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.InsertData(
                table: "MaxVacationLength",
                columns: new[] { "MaxVacationLengthID", "LengthInDays" },
                values: new object[] { 1, 14 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 1,
                columns: new[] { "Password", "Salt" },
                values: new object[] { "5RixmLWEuyr3upJdgXTu3sukpJ09z59HAht+Aviycks=", "jPcZvWXD9z0OvaBqCPI14A==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MaxVacationLength",
                keyColumn: "MaxVacationLengthID",
                keyValue: 1);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Moderated",
                table: "VacationRequestsStatus",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 1,
                columns: new[] { "Password", "Salt" },
                values: new object[] { "OH+OBKCmhlB7ZRyagGRKp7w13eQN19n8O/vA2JVfnzY=", "Gio/D1IUF/4JWR4vCvmPUw==" });
        }
    }
}
