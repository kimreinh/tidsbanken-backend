﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tidsbanken.Migrations
{
    public partial class HashAndSalt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 1,
                columns: new[] { "Password", "Salt" },
                values: new object[] { "OH+OBKCmhlB7ZRyagGRKp7w13eQN19n8O/vA2JVfnzY=", "Gio/D1IUF/4JWR4vCvmPUw==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 1,
                columns: new[] { "Password", "Salt" },
                values: new object[] { "Ri0AeHWyDkVIlWON94u2+2AZElYf/4rjT89hniG1Mbg=", "SomeRandomString" });
        }
    }
}
