﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tidsbanken.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Salt",
                table: "Users",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserID", "Email", "FirstName", "IsAdmin", "LastName", "Password", "ProfileImage", "Salt" },
                values: new object[] { 1, "admin@gmail.com", "admin", true, "admin", "Ri0AeHWyDkVIlWON94u2+2AZElYf/4rjT89hniG1Mbg=", "empty", "SomeRandomString" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserID",
                keyValue: 1);

            migrationBuilder.DropColumn(
                name: "Salt",
                table: "Users");
        }
    }
}
