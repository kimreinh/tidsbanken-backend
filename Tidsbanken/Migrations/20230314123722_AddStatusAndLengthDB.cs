﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tidsbanken.Migrations
{
    public partial class AddStatusAndLengthDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VacationRequests_VacationRequestStatus_VacationRequestStatusID",
                table: "VacationRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_VacationRequestStatus_Users_UserID",
                table: "VacationRequestStatus");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VacationRequestStatus",
                table: "VacationRequestStatus");

            migrationBuilder.RenameTable(
                name: "VacationRequestStatus",
                newName: "VacationRequestsStatus");

            migrationBuilder.RenameIndex(
                name: "IX_VacationRequestStatus_UserID",
                table: "VacationRequestsStatus",
                newName: "IX_VacationRequestsStatus_UserID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_VacationRequestsStatus",
                table: "VacationRequestsStatus",
                column: "VacationRequestStatusID");

            migrationBuilder.AddForeignKey(
                name: "FK_VacationRequests_VacationRequestsStatus_VacationRequestStatusID",
                table: "VacationRequests",
                column: "VacationRequestStatusID",
                principalTable: "VacationRequestsStatus",
                principalColumn: "VacationRequestStatusID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VacationRequestsStatus_Users_UserID",
                table: "VacationRequestsStatus",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VacationRequests_VacationRequestsStatus_VacationRequestStatusID",
                table: "VacationRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_VacationRequestsStatus_Users_UserID",
                table: "VacationRequestsStatus");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VacationRequestsStatus",
                table: "VacationRequestsStatus");

            migrationBuilder.RenameTable(
                name: "VacationRequestsStatus",
                newName: "VacationRequestStatus");

            migrationBuilder.RenameIndex(
                name: "IX_VacationRequestsStatus_UserID",
                table: "VacationRequestStatus",
                newName: "IX_VacationRequestStatus_UserID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_VacationRequestStatus",
                table: "VacationRequestStatus",
                column: "VacationRequestStatusID");

            migrationBuilder.AddForeignKey(
                name: "FK_VacationRequests_VacationRequestStatus_VacationRequestStatusID",
                table: "VacationRequests",
                column: "VacationRequestStatusID",
                principalTable: "VacationRequestStatus",
                principalColumn: "VacationRequestStatusID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VacationRequestStatus_Users_UserID",
                table: "VacationRequestStatus",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "UserID");
        }
    }
}
