﻿using System.ComponentModel.DataAnnotations;

namespace Tidsbanken.Models.Domain
{
    /// <summary>
    /// Specifying the properties for the Comment domain model.
    /// </summary>
    public class Comment
    {
        public int CommentID { get; set; }  // Primary key
        [Required, MaxLength(150)]
        public string Message { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Edited { get; set; }
        public int VacationRequestID { get; set; } // Foreign key
        public int? UserID { get; set; } // Foreign key
        public User? User { get; set; } // Inverse navigation property
        public VacationRequest VacationRequest { get; set; } // Inverse navigation property
    }
}
