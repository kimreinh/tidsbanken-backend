﻿namespace Tidsbanken.Models.Domain
{
    /// <summary>
    /// Specifying the properties for the MaxVacationLength domain model.
    /// </summary>
    public class MaxVacationLength
    {
        public int MaxVacationLengthID { get; set; }  // Primary key
        public int LengthInDays { get; set; }
        public ICollection<VacationRequest> vacationRequests { get; set; } // Collection navigation property (One MaxVacationLength -> Many VacationRequests).
    }
}
