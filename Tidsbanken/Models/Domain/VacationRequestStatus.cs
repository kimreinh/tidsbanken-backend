﻿namespace Tidsbanken.Models.Domain
{
    /// <summary>
    /// Enumeration which defines a set of states for a vacation request.
    /// </summary>
    public enum Status
    {
        Pending,
        Approved,
        Denied
    }

    /// <summary>
    /// Specifying the properties for the VacationRequestStatus domain model.
    /// </summary>
    public class VacationRequestStatus
    {
        public int VacationRequestStatusID { get; set; }
        public Status Status { get; set; }
        public DateTime? Moderated { get; set; }
        public int? UserID { get; set; } // Foregin key
        public User? User { get; set; } // Inverse navigation property
        public VacationRequest VacationRequest { get; set; } // Inverse navigation property
    }
}
