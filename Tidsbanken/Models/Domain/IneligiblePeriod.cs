﻿using System.ComponentModel.DataAnnotations;

namespace Tidsbanken.Models.Domain
{
    /// <summary>
    /// Specifying the properties for the IneligiblePeriod domain model.
    /// </summary>
    public class IneligiblePeriod
    {
        public int IneligiblePeriodID { get; set; } // Primary key
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime PeriodStart { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime PeriodEnd { get; set; }
        public int UserID { get; set; } // Foreign key
        public User User { get; set; } // Inverse navigation property
    }
}
