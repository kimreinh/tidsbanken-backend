﻿using System.ComponentModel.DataAnnotations;

namespace Tidsbanken.Models.Domain
{
    /// <summary>
    /// Specifying the properties for the VacationRequest domain model.
    /// </summary>
    public class VacationRequest
    {
        public int VacationRequestID { get; set; }  // Primary key
        [Required, MaxLength(30)]
        public string Title { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime PeriodStart { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime PeriodEnd { get; set; }
        public int UserID { get; set; } // Foreign key
        public int VacationRequestStatusID { get; set; }  // Foreign key
        public int MaxVacationLengthID { get; set; } = 1; // Foreign key
        public User? User { get; set; } // Inverse navigation property
        public VacationRequestStatus VacationRequestStatus { get; set; }  // Inverse navigation property
        public MaxVacationLength MaxVacationLength { get; set; }  // Inverse navigation property
        public ICollection<Comment>? Comments { get; set; } // Collection navigation property (One VacationRequest -> Many Comments)
    }
}
