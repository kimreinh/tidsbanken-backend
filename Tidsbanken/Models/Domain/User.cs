﻿using System.ComponentModel.DataAnnotations;

namespace Tidsbanken.Models.Domain
{
    /// <summary>
    /// Specifying the properties for the User domain model.
    /// </summary>
    public class User
    {
        public int UserID { get; set; }  // Primary key
        [Required, MaxLength(50)]
        public string FirstName { get; set; }
        [Required, MaxLength(50)]
        public string LastName { get; set; }
        [Required, EmailAddress, MaxLength(80)]
        public string Email { get; set; }
        [Required, MaxLength(50)]
        public string Password { get; set; }
        public string Salt { get; set; }
        public string ProfileImage { get; set; }
        public bool IsAdmin { get; set; }
        public ICollection<VacationRequest>? VacationRequests { get; set; } // Collection navigation property (One User -> Many VacationRequests)
        public ICollection<Comment>? Comments { get; set; } // Collection navigation property (One User -> Many Comments)
        public ICollection<IneligiblePeriod>? IneligiblePeriods { get; set; } // Collection navigation property (One user -> Many IneligiblePeriods)
        public ICollection<VacationRequestStatus>? vacationRequestStatuses { get; set; } // Collection navigation property (One user -> Many VacationRequestStatuses)
    }
}
