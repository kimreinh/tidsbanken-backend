﻿namespace Tidsbanken.Models.DTOs.Comment
{
    public class CommentEditDTO
    {
        public int CommentID { get; set; }
        public string Message { get; set; }
    }
}
