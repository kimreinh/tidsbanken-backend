﻿namespace Tidsbanken.Models.DTOs.Comment
{
    public class CommentCreateDTO
    {
        public string Message { get; set; }
    }
}
