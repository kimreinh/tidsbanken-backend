﻿namespace Tidsbanken.Models.DTOs.Comment
{
    public class CommentReadDTO
    {
        public int CommentID { get; set; }
        public string Message { get; set; }
        public DateTime Created { get; set; }
        public DateTime Edited { get; set; }
        public int? UserID { get; set; }
    }
}
