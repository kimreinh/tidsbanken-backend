﻿namespace Tidsbanken.Models.DTOs.User
{
    public class UserReadDTO : UserReadParentDTO
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ProfileImage { get; set; }
        public bool IsAdmin { get; set; }
        public List<int> VacationRequests { get; set; }
        public List<int> Comments { get; set; }
        public List<int> IneligiblePeriods { get; set; }
        public List<int> VacationRequestStatuses { get; set; }
    }
}
