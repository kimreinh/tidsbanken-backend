﻿namespace Tidsbanken.Models.DTOs.User
{
    public class UserEditDTO
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ProfileImage { get; set; }
        public bool IsAdmin { get; set; }
    }
}
