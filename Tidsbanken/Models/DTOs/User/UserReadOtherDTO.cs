﻿namespace Tidsbanken.Models.DTOs.User
{
    public class UserReadOtherDTO : UserReadParentDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfileImage { get; set; }
    }
}
