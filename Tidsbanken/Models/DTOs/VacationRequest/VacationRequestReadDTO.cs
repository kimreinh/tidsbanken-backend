﻿using System.ComponentModel.DataAnnotations;
using Tidsbanken.Models.Domain;

namespace Tidsbanken.Models.DTOs.VacationRequest
{
    public class VacationRequestReadDTO
    {
        public int VacationRequestID { get; set; }
        public string Title { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public List<int> Comments { get; set; }
        public int UserID { get; set; }
        public int VacationRequestStatusID { get; set; }
    }
}