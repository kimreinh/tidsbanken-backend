﻿using Tidsbanken.Models.Domain;

namespace Tidsbanken.Models.DTOs.VacationRequest
{
    public class VacationRequestStatusReadDTO
    {
        public int VacationRequestStatusID { get; set; }
        public Status Status { get; set; }
        public DateTime? Moderated { get; set; }
        public int? UserID { get; set; } // Foregin key
    }
}
