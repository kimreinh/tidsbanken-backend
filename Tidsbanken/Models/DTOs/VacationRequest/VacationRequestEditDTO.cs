﻿using System.ComponentModel.DataAnnotations;
using Tidsbanken.Models.Domain;

namespace Tidsbanken.Models.DTOs.VacationRequest
{
    public class VacationRequestEditDTO
    {
        public int VacationRequestID { get; set; }
        public string Title { get; set; }
        public Status Status { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
    }
}
