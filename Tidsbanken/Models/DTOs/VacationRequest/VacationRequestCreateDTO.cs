﻿using System.ComponentModel.DataAnnotations;
using Tidsbanken.Models.Domain;

namespace Tidsbanken.Models.DTOs.VacationRequest
{
    public class VacationRequestCreateDTO
    {
        public string Title { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime PeriodStart { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime PeriodEnd { get; set; }
    }
}
