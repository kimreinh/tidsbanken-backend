﻿namespace Tidsbanken.Models.DTOs.IneligiblePeriod
{
    public class IneligiblePeriodCreateDTO
    {
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
    }
}
