﻿namespace Tidsbanken.Models.DTOs.IneligiblePeriod
{
    public class IneligiblePeriodEditDTO
    {
        public int IneligiblePeriodId { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public int UserID { get; set; }
    }
}
