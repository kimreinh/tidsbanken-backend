﻿namespace Tidsbanken.Models
{
    /// <summary>
    /// Represents a response from an authentication system, where the "Token" property contains an authentication token 
    /// that can be used to authenticate subsequent requests.
    /// </summary>
    public class AuthenticatedResponse
    {
        public string? Token { get; set; }
    }
}
