﻿using AutoMapper;
using Tidsbanken.Models.Domain;
using Tidsbanken.Models.DTOs.IneligiblePeriod;

namespace Tidsbanken.Profiles
{
    public class IneligiblePeriodProfile : Profile
    {
        public IneligiblePeriodProfile()
        {
            // Defing a mapping from IneligibilePeriod to IneligiblePeriodReadDTO
            CreateMap<IneligiblePeriod, IneligiblePeriodReadDTO>();

            // Define a mapping from IneligiblePeriodCreateDTO to IneligiblePeriod
            CreateMap<IneligiblePeriodCreateDTO, IneligiblePeriod>();

            // Define a mapping from IneligiblePeriodEditDTO to IneligiblePeriod
            CreateMap<IneligiblePeriodEditDTO, IneligiblePeriod>();
        }
    }
}
