﻿using AutoMapper;
using Tidsbanken.Models.Domain;
using Tidsbanken.Models.DTOs.User;

namespace Tidsbanken.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile() 
        {
            // Define a mapping from User to UserReadDTO
            CreateMap<User, UserReadDTO>()
                // Turning related vacation requests into arrays
                .ForMember(udto => udto.VacationRequests, opt => opt
                .MapFrom(u => u.VacationRequests.Select(u => u.VacationRequestID).ToArray()))
                // Turning related comments into arrays
                .ForMember(udto => udto.Comments, opt => opt
                .MapFrom(u => u.Comments.Select(u => u.CommentID).ToArray()))
                // Turning related ineligible periods into arrays
                .ForMember(udto => udto.IneligiblePeriods, opt => opt
                .MapFrom(u => u.IneligiblePeriods.Select(u => u.IneligiblePeriodID).ToArray()))
                // Turning related vacation request statuses into arrays
                .ForMember(udto => udto.VacationRequestStatuses, opt => opt
                .MapFrom(u => u.vacationRequestStatuses.Select(u => u.VacationRequestStatusID).ToArray()));

            // Define a mapping from UserCreateDTO to User
            CreateMap<UserCreateDTO, User>();
            // Define a mapping from UserEditDTO to User
            CreateMap<UserEditDTO, User>();
            // Define a mapping from UserLoginDTO to User
            CreateMap<UserLoginDTO, User>();
            // Define a mapping from User to UserReadOtherDTO
            CreateMap<User, UserReadOtherDTO>();
        }
    }
}
