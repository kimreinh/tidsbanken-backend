﻿using AutoMapper;
using Tidsbanken.Models.Domain;
using Tidsbanken.Models.DTOs.Comment;

namespace Tidsbanken.Profiles
{
    public class CommentProfile : Profile
    {
        public CommentProfile()
        {
            // Defining a mapping from Comment to CommentReadDTO and enabling two-way mapping
            CreateMap<Comment, CommentReadDTO>()
                .ReverseMap();

            // Defing a mapping from CommentCreateDTO to Comment and enabling two-way mapping
            CreateMap<CommentCreateDTO, Comment>()
                .ReverseMap();

            // Defing a mapping from CommentEditDTO to Comment and enabling two-way mapping
            CreateMap<CommentEditDTO, Comment>()
                .ReverseMap();
        }
    }
}
