﻿using AutoMapper;
using NuGet.Protocol;
using Tidsbanken.Models.Domain;
using Tidsbanken.Models.DTOs.VacationRequest;

namespace Tidsbanken.Profiles
{
    public class VacationRequestProfile : Profile
    {
        public VacationRequestProfile() 
        {
            // Define a mapping from VacationRequest to VacationRequestReadDTO
            CreateMap<VacationRequest, VacationRequestReadDTO>()
                .ForMember(vdto => vdto.Comments, opt => opt
                .MapFrom(v => v.Comments.Select(v => v.CommentID).ToArray()))
                .ReverseMap();

            // Define a mapping from VacationRequestCreateDTO to VacationRequest
            CreateMap<VacationRequestCreateDTO, VacationRequest>()
                .ReverseMap();

            // Define a mapping from VacationRequestEditDTO to VacationRequest
            CreateMap<VacationRequestEditDTO, VacationRequest>()
                .ReverseMap();
        }
    }
}
