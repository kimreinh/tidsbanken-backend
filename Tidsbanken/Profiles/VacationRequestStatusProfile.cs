﻿using AutoMapper;
using NuGet.Protocol;
using Tidsbanken.Models.Domain;
using Tidsbanken.Models.DTOs.VacationRequest;

namespace Tidsbanken.Profiles
{
    public class VacationRequestStatusProfile : Profile
    {
        public VacationRequestStatusProfile()
        {
            // Define a mapping from VacationRequestStatus to VacationRequestStatusReadDTO
            CreateMap<VacationRequestStatus, VacationRequestStatusReadDTO>();
        }
    }
}
