﻿using System.Net.Mime;
using System.Security.Claims;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Tidsbanken.Models.Domain;
using Tidsbanken.Models.DTOs.IneligiblePeriod;
using Tidsbanken.Services;

namespace Tidsbanken.Controllers
{
    [Route("api/ineligible")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [Authorize]
    public class IneligiblePeriodsController : ControllerBase
    {
        private readonly IIneligiblePeriodService _periodService;
        private readonly IMapper _mapper;

        public IneligiblePeriodsController(IIneligiblePeriodService periodService, IMapper mapper)
        {
            _mapper = mapper;
            _periodService = periodService;
        }

        /// <summary>
        /// Get all ineligible periods.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<IneligiblePeriodReadDTO>>> GetIneligiblePeriods()
        {
            return _mapper.Map<List<IneligiblePeriodReadDTO>>(await _periodService.GetIneligiblePeriodsAsync());
        }

        /// <summary>
        /// Get ineligible period by period id.
        /// </summary>
        /// <param name="periodId"></param>
        /// <returns></returns>
        [HttpGet("{periodId}")]
        public async Task<ActionResult<IneligiblePeriodReadDTO>> GetIneligiblePeriod(int periodId)
        {
            if (!_periodService.IneligiblePeriodExists(periodId)) { return NotFound(); }
            return _mapper.Map<IneligiblePeriodReadDTO>(await _periodService.GetIneligiblePeriodByIdAsync(periodId));
        }

        /// <summary>
        /// Add a new ineligible period.
        /// </summary>
        /// <param name="ineligiblePeriod"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<IneligiblePeriod>> PostIneligiblePeriod(IneligiblePeriodCreateDTO ineligiblePeriod)
        {
            var userId = int.Parse(HttpContext.User.FindFirstValue("userID"));
            IneligiblePeriod domainPeriod = _mapper.Map<IneligiblePeriod>(ineligiblePeriod);
            domainPeriod.UserID = userId;

            await _periodService.PostIneligiblePeriodAsync(domainPeriod);
            return Ok();
        }

        /// <summary>
        /// Delete ineligible period.
        /// </summary>
        /// <param name="periodId"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{periodId}")]
        public async Task<IActionResult> DeleteIneligiblePeriod(int periodId)
        {
            if (!_periodService.IneligiblePeriodExists(periodId)) { return NotFound(); }
            await _periodService.DeleteIneligiblePeriodAsync(periodId);

            return NoContent();
        }

        /// <summary>
        /// Partial update to ineligible vacation period in db.
        /// </summary>
        /// <param name="periodId"></param>
        /// <param name="periodUpdate"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPatch("{periodId}")]
        public async Task<IActionResult> PatchIneligiblePeriod(int periodId, [FromBody] JsonPatchDocument<IneligiblePeriod> periodUpdate)
        {
            if (!_periodService.IneligiblePeriodExists(periodId)) { return NotFound(); }
            if (!periodUpdate.Operations.Any(op => op.path == "/UserID"))
            {
            await _periodService.PatchIneligiblePeriodAsync(periodId, periodUpdate);
            return Ok();
            }
            return Forbid();

        }
    }
}
