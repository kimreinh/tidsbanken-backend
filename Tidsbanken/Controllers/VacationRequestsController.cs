﻿using System.Net.Mime;
using System.Security.Claims;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Tidsbanken.Models.Domain;
using Tidsbanken.Models.DTOs.VacationRequest;
using Tidsbanken.Services;

namespace Tidsbanken.Controllers
{
    [Route("api/requests")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [Authorize]
    public class VacationRequestsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IVacationRequestService _vacationRequestService;

        public VacationRequestsController(IMapper mapper, IVacationRequestService vacationRequestService)
        {
            _mapper = mapper;
            _vacationRequestService = vacationRequestService;
        }

        /// <summary>
        /// Get all vacation requests from the database.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VacationRequestReadDTO>>> GetVacationRequests() 
        {
            // Current authenticated user's id.
            int currentId = int.Parse(HttpContext.User.FindFirstValue("UserID"));

            // Initial empty list of requests that the user should be allowed to see.
            List<VacationRequest> permittedRequests = new List<VacationRequest>();

            // An admin should be able to see any request.
            if (User.IsInRole("Admin"))
            {
                return _mapper.Map<List<VacationRequestReadDTO>>(await _vacationRequestService.GetVacationRequestsAsync());
            }

            foreach (var request in await _vacationRequestService.GetVacationRequestsAsync())
            {
                VacationRequestStatus status = await _vacationRequestService.GetVacationRequestStatusAsync(request.VacationRequestID);
                if (request.UserID == currentId || status.Status == Status.Approved)
                {
                    permittedRequests.Add(request);
                }
            }
            return _mapper.Map<List<VacationRequestReadDTO>>(permittedRequests);
        }

        /// <summary>
        /// Get a specific vacation request by id.
        /// </summary>
        /// <param name="requestId">The id of the vacation request to be retrieved from the database.</param>
        /// <returns></returns>
        [HttpGet("{requestId}")]
        public async Task<ActionResult<VacationRequestReadDTO>> GetVacationRequest(int requestId)
        {
            // Authenticated user's id.
            int currentId = int.Parse(HttpContext.User.FindFirstValue("UserID"));
            VacationRequest domainRequest = await _vacationRequestService.GetSpecificVacationRequestAsync(requestId);
            VacationRequestStatus status = await _vacationRequestService.GetVacationRequestStatusAsync(requestId);
            if (domainRequest == null)
            {
                return NotFound();
            }

            // An admin or the owner of a request should be able to retrieve any request.
            if (User.IsInRole("Admin") || domainRequest.UserID == currentId)
            {
                return _mapper.Map<VacationRequestReadDTO>(domainRequest);
            }
            // If the request belongs to another user and has status set as 'Approved', the request will be retrieved.
            else if (domainRequest.UserID != currentId && status.Status == Status.Approved)
            {
                return _mapper.Map<VacationRequestReadDTO>(domainRequest);
            }
            // Otherwise, they will be unathorized to get the vacation request.
            return Forbid();
        }

        /// <summary>
        /// Create a new vacation request.
        /// </summary>
        /// <param name="dtoRequest">A modified vacationRequest object.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<VacationRequest>> PostVacationRequest(VacationRequestCreateDTO dtoRequest)
        {
            int len = await _vacationRequestService.GetMaxVacationLength();
            TimeSpan t = dtoRequest.PeriodEnd - dtoRequest.PeriodStart;
            if (t.Days > len)
            {
                return BadRequest("Max Vacation length exceeded.");
            }
            // Authenticated user's id.
            int currentId = int.Parse(HttpContext.User.FindFirstValue("UserID"));
            VacationRequest domainRequest = _mapper.Map<VacationRequest>(dtoRequest);
            domainRequest.UserID = currentId;
            domainRequest = await _vacationRequestService.AddVacationRequestAsync(domainRequest);

            return CreatedAtAction(nameof(GetVacationRequest), 
                new { requestId = domainRequest.VacationRequestID },
                _mapper.Map<VacationRequestReadDTO>(domainRequest));
        }

        /// <summary>
        /// Delete a vacation request by id.
        /// </summary>
        /// <param name="requestId">The id of the vacation request to be deleted.</param>
        /// <returns></returns>
        [HttpDelete("{requestId}")]
        public async Task<IActionResult> DeleteVacationRequest(int requestId)
        {
            if (!_vacationRequestService.VacationRequestExists(requestId))
            {
                return NotFound();
            }

            await _vacationRequestService.DeleteVacationRequestAsync(requestId);
            return NoContent();
        }

        /// <summary>
        /// Partial update of a vacation request by id.
        /// </summary>
        /// <param name="requestId">The id of the request that is being updated.</param>
        /// <param name="requestUpdate">A collection of operations to be performed on the target JSON document.</param>
        /// <returns></returns>
        [HttpPatch("{requestId}")]
        public async Task<ActionResult> UpdateVacationRequest(int requestId, [FromBody] JsonPatchDocument<VacationRequest> requestUpdate)
        {
            // Get the current authenticated user's id.
            int currentId = int.Parse(HttpContext.User.FindFirstValue("UserID"));
            VacationRequest request = await _vacationRequestService.GetSpecificVacationRequestAsync(requestId);
            VacationRequestStatus status = await _vacationRequestService.GetVacationRequestStatusAsync(requestId);

            // The user should not be able to update the request if it has been moderated by an admin.
            if (status.Status != Status.Pending)
            {
                return Forbid();
            }

            // Restrict access if the request belongs to another user or the authenticated user is not an admin.
            if ((request.UserID == currentId || User.IsInRole("Admin")) && requestUpdate.Operations.Any())
            {
                await _vacationRequestService.PatchVacationRequestAsync(requestId, requestUpdate);
                return Ok();
            }
            return Forbid();
            
        }

        /// <summary>
        /// Partial update of a vacation request status by requestId.
        /// </summary>
        /// <param name="requestId">The id of the request that is being updated.</param>
        /// <param name="statusUpdate">A collection of operations to be performed on the target JSON document.</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPatch("{requestId}/status")]
        public async Task<ActionResult> UpdateRequestStatus(int requestId, [FromBody] JsonPatchDocument<VacationRequestStatus> statusUpdate)
        {
            if (!_vacationRequestService.VacationRequestExists(requestId)) { return NotFound(); }

            // Get the current authenticated user's id.
            int currentId = int.Parse(HttpContext.User.FindFirstValue("UserID"));
            VacationRequestStatus status = await _vacationRequestService.GetVacationRequestStatusAsync(requestId);

            status.UserID = currentId;
            status.Moderated = DateTime.Now;

            await _vacationRequestService.UpdateRequestStatusAsync(status, statusUpdate);
            return Ok();
        }

        /// <summary>
        /// Updates the maximum vacation length variable in db
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{length}/max-vacation-length")]
        public async Task<ActionResult> UpdateMaxVacationLength(int length)
        {
            await _vacationRequestService.UpdateMaxVacationLengthAsync(length);
            return NoContent();
        }

        /// <summary>
        /// Get max vacation length.
        /// </summary>
        /// <returns></returns>
        [HttpGet("max-vacation-length")]
        public async Task<int> GetMaxVacationLength()
        {
            return await _vacationRequestService.GetMaxVacationLengthAsync();
        }

        /// <summary>
        /// Get a vacation request status by requestId.
        /// </summary>
        /// <param name="requestId">The id of the request to get the status from.</param>
        /// <returns></returns>
        [HttpGet("{requestId}/vacation-request-status")]
        public async Task<ActionResult<VacationRequestStatusReadDTO>> GetVacationRequestStatus(int requestId)
        {
            if (!_vacationRequestService.VacationRequestExists(requestId)) { return NotFound(); }
            return _mapper.Map<VacationRequestStatusReadDTO>(await _vacationRequestService.GetVacationRequestStatusAsync(requestId));
        }
    }
}
