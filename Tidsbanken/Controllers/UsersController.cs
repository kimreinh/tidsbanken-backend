﻿using System.Net.Mime;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tidsbanken.Models.Domain;
using Microsoft.AspNetCore.JsonPatch;
using Tidsbanken.Models.DTOs.User;
using Tidsbanken.Services;
using AutoMapper;
using Tidsbanken.Models.DTOs.VacationRequest;
using Tidsbanken.Tools;

namespace Tidsbanken.Controllers
{
    [Route("api/users")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [Authorize]

    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly IVacationRequestService _vacationRequestService;

        public UsersController(IUserService userService, IMapper mapper, IVacationRequestService vacationRequestService)
        {
            _userService = userService;
            _mapper = mapper;
            _vacationRequestService = vacationRequestService;
        }

        /// <summary>
        /// Get all users.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUsers()
        {
            return _mapper.Map<List<UserReadDTO>>(await _userService.GetAllUsersAsync());
        }

        /// <summary>
        /// Get a specific user by id.
        /// </summary>
        /// <param name="userId">The id of the user to retrieve information from.</param>
        /// <returns></returns>
        [HttpGet("{userId}")]
        public async Task<ActionResult<UserReadParentDTO>> GetUser(int userId)
        {
            // Retrieving the id of the current authenticated user.
            int currentId = int.Parse(HttpContext.User.FindFirstValue("UserID"));
            User user = await _userService.GetSpecificUserAsync(userId);

            if (user == null)
            {
                return NotFound();
            }

            if (userId != currentId)
            {
                return _mapper.Map<UserReadOtherDTO>(user);
            }

            return _mapper.Map<UserReadDTO>(user);
        }

        /// <summary>
        /// Add a user to the database.
        /// </summary>
        /// <param name="dtoUser">A user domain object.</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(UserCreateDTO dtoUser)
        {
            User domainUser = _mapper.Map<User>(dtoUser);
            domainUser.Salt = Password.GenerateSalt();
            domainUser.Password = Password.HashPassword(dtoUser.Password, domainUser.Salt);

            domainUser = await _userService.AddUserAsync(domainUser);

            return CreatedAtAction(nameof(GetUser),
                new { userId = domainUser.UserID },
                _mapper.Map<UserReadDTO>(domainUser));
        }

        /// <summary>
        /// Delete a user from the database.
        /// </summary>
        /// <param name="userId">The id of the user to be deleted.</param>
        /// <returns></returns>
        [HttpDelete("{userId}")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            // Authenticated user's id.
            int currentId = int.Parse(HttpContext.User.FindFirstValue("UserID"));
            User user = await _userService.GetSpecificUserAsync(userId);

            if (!_userService.UserExists(userId))
            {
                return NotFound();
            }

            // Only an admin or the owner itself can delete the user.
            if (User.IsInRole("Admin") || userId == currentId)
            {
                // Iterating over each request owned by the user.
                // This has to be done to make sure an admin can be deleted when they have moderated a vacation request.
                foreach(var vacationRequest in user.VacationRequests)
                {
                    await _vacationRequestService.DeleteVacationRequestAsync(vacationRequest.VacationRequestID);
                }
                await _userService.DeleteUserAsync(userId);
                return NoContent();
            }

            return Forbid();
        }

        /// <summary>
        /// Partial update of a user in the database.
        /// </summary>
        /// <param name="userId">The id of the user to be updated.</param>
        /// <param name="userUpdate">A collection of operations to be performed on the target JSON document.</param>
        /// <returns></returns>
        [HttpPatch("user/{userId}")]
        public async Task<IActionResult> PatchUser(int userId, [FromBody] JsonPatchDocument<User> userUpdate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Authenticated user's id.
            int currentId = int.Parse(HttpContext.User.FindFirstValue("UserID"));

            // Only admins may edit the isAdmin property.
            if (!User.IsInRole("Admin") && userUpdate.Operations.Any(op => op.path == "/isAdmin"))
            {
                return Forbid();
            }

            // No one should be able to use this method to change the password.
            if (userUpdate.Operations.Any(op => op.path == "/password"))
            {
                return BadRequest();
            }

            // Other users should not be able to change the user attributes.
            if ((userId == currentId || User.IsInRole("Admin")) && userUpdate.Operations.Any())
            {
                await _userService.PatchUserAsync(userId, userUpdate);

                return Ok();
            }
            return Forbid();

        }

        /// <summary>
        /// Get all vacation requests for a specific user by id.
        /// </summary>
        /// <param name="userId">The id of the user to retrieve information from.</param>
        /// <returns></returns>
        [HttpGet("user/{userId}/requests")]
        public async Task<ActionResult<List<VacationRequestReadDTO>>> GetRequestsByUser(int userId)
        {
            // Current authenticated user's id.
            int currentId = int.Parse(HttpContext.User.FindFirstValue("UserID"));

            // Initial empty list of requests that the user should be allowed to see.
            List<VacationRequest> permittedRequests = new List<VacationRequest>();

            if (!_userService.UserExists(userId))
            {
                return NotFound();
            }

            try
            {
                if (User.IsInRole("Admin"))
                {
                    return _mapper.Map<List<VacationRequestReadDTO>>(await _userService.GetRequestsByUserAsync(userId));
                }

                foreach (var request in await _userService.GetRequestsByUserAsync(userId))
                {
                    VacationRequestStatus status = await _vacationRequestService.GetVacationRequestStatusAsync(request.VacationRequestID);
                    if (request.UserID == currentId || status.Status == Status.Approved)
                    {
                        permittedRequests.Add(request);
                    }
                }
                return _mapper.Map<List<VacationRequestReadDTO>>(permittedRequests);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid user");
            }
        }

        /// <summary>
        /// Password change for the currently authenticated user.
        /// </summary>
        /// <param name="oldPassword">The previous password.</param>
        /// <param name="newPassword">The new updated password.</param>
        /// <returns></returns>
        [HttpPost("user/update-password")]
        public async Task<ActionResult> ChangePassword(string oldPassword, string newPassword)
        {
            int currentId = int.Parse(HttpContext.User.FindFirstValue("UserID"));
            
            User user = await _userService.GetSpecificUserAsync(currentId);
            if (user.Password == Password.HashPassword(oldPassword, user.Salt))
            {
                await _userService.ChangePasswordAsync(user, newPassword);
                return NoContent();
            }
            return BadRequest("Wrong password entered, try again!");
        }
    }
}
