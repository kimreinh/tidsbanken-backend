﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Mime;
using System.Security.Claims;
using Tidsbanken.Models;
using Tidsbanken.Models.Domain;
using Tidsbanken.Models.DTOs.User;
using Tidsbanken.Services;
using Tidsbanken.Tools;

namespace Tidsbanken.Controllers
{
    [Route("api/auth")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class AuthController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IAuthenticationService _authenticationService;

        public AuthController(IMapper mapper, IAuthenticationService authenticationService)
        {
            _mapper = mapper;
            _authenticationService = authenticationService;
        }

        /// <summary>
        /// Authenticates a user and generates an access token for accessing protected resources.
        /// </summary>
        /// <param name="dtoUserLogin">A modified user object.</param>
        /// <returns>A JWT Bearer token.</returns>
        [HttpPost("login")]
        public async Task<IActionResult> Login(UserLoginDTO dtoUserLogin)
        {
            User dbUser = _mapper.Map<User>(dtoUserLogin);
            try
            {
                dbUser = await _authenticationService.LoginAsync(dbUser);
            }
            catch (Exception)
            {
                return Unauthorized("Username or password is incorrect");
            }

            if (dbUser != null)
            {
                string password = Password.HashPassword(dtoUserLogin.Password, dbUser.Salt);

                if (password == dbUser.Password)
                {
                    List<Claim> authClaims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Email, dbUser.Email),
                        new Claim(ClaimTypes.NameIdentifier, dbUser.UserID.ToString()),
                        new Claim("userID", dbUser.UserID.ToString())
                    };


                    if (dbUser.IsAdmin == true)
                    {
                        authClaims.Add(new Claim(ClaimTypes.Role, "Admin"));
                        authClaims.Add(new Claim("Role", "Admin"));
                    }

                    var token = _authenticationService.GetToken(authClaims);

                    var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

                    return Ok(new AuthenticatedResponse { Token = tokenString });
                }
            }

            return Unauthorized("Username or password is incorrect");
        }
    }
}
