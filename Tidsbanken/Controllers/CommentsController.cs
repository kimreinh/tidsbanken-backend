﻿using System.Net.Mime;
using System.Security.Claims;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Tidsbanken.Models.Domain;
using Tidsbanken.Models.DTOs.Comment;
using Tidsbanken.Services;

namespace Tidsbanken.Controllers
{
    [Route("api/comments")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [Authorize]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly IVacationRequestService _vacationRequestService;
        private readonly IMapper _mapper;

        public CommentsController(ICommentService commentService, IMapper mapper, IVacationRequestService vacationRequestService)
        {
            _commentService = commentService;
            _mapper = mapper;
            _vacationRequestService = vacationRequestService;
        }

        /// <summary>
        /// Get comments by vacation request ID.
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        [HttpGet("{requestId}/comment")]
        public async Task<ActionResult<IEnumerable<CommentReadDTO>>> GetComments(int requestId)
        {
            // Get the current authenticated user's id.
            var userId = int.Parse(HttpContext.User.FindFirstValue("userID"));
            var vacationRequest = await _commentService.GetVacationRequest(requestId);
            
            // Only the owner of a vacation request or an admin should be able to view the comments.
            if (vacationRequest != null)
            {
                if (userId == vacationRequest.UserID || User.IsInRole("Admin"))
                {
                    return _mapper.Map<List<CommentReadDTO>>(await _commentService.GetAllRequestCommentsAsync(requestId));
                }
            }
            return Forbid();
        }

        /// <summary>
        /// Get a specific comment by comment's ID.
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="commentId"></param>
        /// <returns></returns>
        [HttpGet("{requestId}/comment/{commentId}")]
        public async Task<ActionResult<CommentReadDTO>> GetCommentById(int requestId, int commentId)
        {
            // Get the current authenticated user's id.
            var userId = int.Parse(HttpContext.User.FindFirstValue("userID"));
            if (!_commentService.CommentExists(commentId)) { return NotFound(); }

            VacationRequest request = await _vacationRequestService.GetSpecificVacationRequestAsync(requestId);
            int vacationRequestOwnerID = request.UserID;
            // Only administrators or request owners can view comments.
            if (User.IsInRole("Admin") || userId == vacationRequestOwnerID)
            {
                return _mapper.Map<CommentReadDTO>(await _commentService.GetCommentAsync(commentId));
            }
            // Restrict access to the comment if user is not authorized.
            return Forbid();
        }

        /// <summary>
        /// Add a new comment to a vacation request.
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        [HttpPost("{requestId}/comment")]
        public async Task<ActionResult<Comment>> PostComment(int requestId, CommentCreateDTO comment)
        {
            // Get the current authenticated user's id.
            var userId = int.Parse(HttpContext.User.FindFirstValue("UserID"));
            VacationRequest request = await _commentService.GetVacationRequest(requestId);
            Comment domainComment = _mapper.Map<Comment>(comment);
            if (comment.Message.Length > 150) { return BadRequest(); }

            // Only administrators or request owners can create comments.
            if (userId == request.UserID || User.IsInRole("Admin"))
            {
                domainComment.VacationRequestID = requestId;
                domainComment.UserID = userId;
                domainComment.Created = DateTime.Now;

                await _commentService.PostCommentAsync(domainComment);

                return CreatedAtAction(nameof(GetCommentById), 
                    new { requestId = domainComment.VacationRequestID, commentId = domainComment.CommentID},
                    _mapper.Map<CommentReadDTO>(domainComment));
            }
            // Other users do not have permission to create comments.
            return Forbid();
        }

        /// <summary>
        /// Delete a comment from the database.
        /// </summary>
        /// <param name="requestId">The id of a given request.</param>
        /// <param name="commentId">The id of the comment that is being deleted.</param>
        /// <returns></returns>
        [HttpDelete("{requestId}")]
        public async Task<IActionResult> DeleteComment(int requestId, int commentId)
        {
            if (!_commentService.CommentExists(commentId)) { return NotFound(); }
            var userId = int.Parse(HttpContext.User.FindFirstValue("userID"));
            VacationRequest request = await _commentService.GetVacationRequest(requestId);
            Comment comment = await _commentService.GetCommentAsync(commentId);
            
            // Check if user has role admin or is the request owner.
            if (userId == request.UserID || User.IsInRole("Admin"))
            {
                // Comments may only be deleted within the first 24 hours.
                if (DateTime.Now - comment.Created > TimeSpan.FromHours(24))
                {
                    return Forbid();
                }
                await _commentService.DeleteCommentAsync(commentId);
                return NoContent();
            }
            return Forbid();
        }

        /// <summary>
        /// Partial update of a comment by requestId.
        /// </summary>
        /// <param name="requestId">The id of the request</param>
        /// <param name="commentId">The id of the comment that is being updated</param>
        /// <param name="commentUpdate">A collection of operations to be performed on the target JSON document.</param>
        /// <returns></returns>
        [HttpPatch("{requestId}/comment/{commentId}")]
        public async Task<IActionResult> PatchComment(int requestId, int commentId, [FromBody] JsonPatchDocument<Comment> commentUpdate)
        {
            int currentId = int.Parse(HttpContext.User.FindFirstValue("UserID"));
            if (!_commentService.CommentExists(commentId)) { return NotFound(); }

            VacationRequest request = await _vacationRequestService.GetSpecificVacationRequestAsync(requestId);
            int vacationRequestOwnerID = request.UserID;

            Comment comment = await _commentService.GetCommentAsync(commentId);

            if (vacationRequestOwnerID == currentId && commentUpdate.Operations.Any())
            {
                // Comments may only be edited within the first 24 hours.
                if (DateTime.Now - comment.Created < TimeSpan.FromHours(24))

                    comment.Edited = DateTime.Now;
                    await _commentService.PatchCommentAsync(comment, commentUpdate);
                    return Ok();
            }
            // Restrict access to the comment if user is not authorized.
            return Forbid();
        }
    }
}