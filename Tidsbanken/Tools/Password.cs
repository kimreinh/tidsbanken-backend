﻿using System.Security.Cryptography;
using System.Text;

namespace Tidsbanken.Tools
{
    public class Password
    {
        /// <summary>
        /// Securely hashing a user's password before storing it in a database or comparing it to a previously hashed password.
        /// </summary>
        /// <param name="password">The password of a user.</param>
        /// <param name="salt">A random value that is added to a password before it is hashed.</param>
        /// <returns></returns>
        public static string HashPassword(string password, string salt)
        {
            var sha = SHA256.Create();                                      // SHA-256 cryptographic hashing algorithm
            var asByteArray = Encoding.Default.GetBytes(password + salt);   // Converting the password and the salt into a byte array
            var hashedPassword = sha.ComputeHash(asByteArray);              // Computing the output of the hash function (hash digest)
            return Convert.ToBase64String(hashedPassword);                  // Converting the hash digest into a Base64-encoded string
        }

        /// <summary>
        /// Generates a random salt value and returns it as a Base64-encoded string
        /// </summary>
        /// <returns></returns>
        public static string GenerateSalt()
        {
            int saltSize = 16;
            var salt = RandomNumberGenerator.GetBytes(saltSize);
            return Convert.ToBase64String(salt);
        }
    }
}
