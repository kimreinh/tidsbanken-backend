﻿using Microsoft.EntityFrameworkCore;
using Tidsbanken.Models.Domain;
using Tidsbanken.Tools;

namespace Tidsbanken.Data
{
    public class TidsbankenDbContext : DbContext
    {
        public TidsbankenDbContext(DbContextOptions options) : base(options) { }

        // DbSets for the tables in the database.
        public DbSet<Comment> Comments { get; set; }
        public DbSet<IneligiblePeriod> IneligiblePeriods { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<VacationRequest> VacationRequests { get; set; }
        public DbSet<VacationRequestStatus> VacationRequestsStatus { get; set; }
        public DbSet<MaxVacationLength> MaxVacationLength { get; set; }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            string salt = Password.GenerateSalt();
            string password = "admin";
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();  // Making sure that the Email property for a user must be unique.

            // Setting up the initial administrator, which is required to create new users.
            modelBuilder.Entity<User>().HasData(
                 new User
                 {
                     UserID = 1,
                     FirstName = "admin",
                     LastName = "admin",
                     Email = "admin@gmail.com",
                     Salt = salt,
                     Password = Password.HashPassword(password, salt),
                     ProfileImage = "empty",
                     IsAdmin = true
                 });

            // Setting up initial values for Maximum vacation length.
            modelBuilder.Entity<MaxVacationLength>().HasData(
                 new MaxVacationLength
                 {
                     MaxVacationLengthID = 1,
                     LengthInDays = 14
                 });
        }
    }
}
